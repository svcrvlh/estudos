# ESTUDOS

Conhecimento em JPA

Conhecimento em SPARk

Conhecimento em banco de dados Relacionais

Conhecimento em Padrões de arquitetura, ex: Lambda, CQRS, OutBox, Kappa

Clean architecture

Clean Code

Padões de projeto e como implementar no dia a dia, ser criterioso no code review 
como intuito de criar a cultura de utilização de padrões

EMR

infraestruture as code

modelagem de dados

Criar uma visão de futuro para a ferramenta

SOLID

Spring(Spring boot, data, security, batch, cloud)

Blue green deployment

Canary release

Kubernetes

## Tópicos
> PNL - ser flexivel, mais 

> flexibilidade

> otimismo

> dominar techradar

> system design

## **2021-10-18**

### Lambda

*Servless* - Aplicativo e computaçãos em servidor

* Cobrado por vezes que a sua função for executada
* Mais barato que uma instancia rodando 24x7
* Não precisa se preocupar com gerenciamento do servidor
* Foco total na aplicação
* Suporte: C#, Go, Java, Node, Pyton
* Escalabilidade
* Disponibilidade

É disparado via trigger/evento
* Serviços da AWS

Output - CloudWatch

Links:

[Do more with less.
‍Serverless.](https://www.serverless.com/)

[Serverless com NodeJS e AWS Lambda | Diego Fernandes](https://www.youtube.com/watch?v=jiP45rEOEbA)

[Serverless: Quando utilizar e aplicações com NodeJS](https://blog.rocketseat.com.br/serverless-nodejs-lambda/)

## **2021-10-13**

### Kubernetes

Gerencia uma ou mais máquinas(cluster), orquestrador de containers

**POD** - recurso que encapsula um container

**Master**
* gerencia os cluster
* Mantem e atualiza o status desejado
* Recebe e executa novos comandos

**Nodes** - executa as aplicações

**API**
* Gerencia
* kubectl - cria, ler, atualiza e remove

**Service** 

## **2021-10-09**

### SOLID

Benefício da orientação a objetos:
* Fácil de manter, adaptar e se ajustar as alterações de escopo
* Testável e de fácil entendimento
* Estensível para alterações com o menor esforço necessário
* Máximo de reaproveitamento
* Permanecer o máximo de tempo possível em utilização

Evitar problemas comuns:
* Dificuldade na testabilidade / criação de testes de unidade
* Código sem estrutura ou padrão
* Dificuldade de isolar funcionalidades
* Duplicação de código
* Fragilidade, o códi3go quebra facilmente em vários pontos após alguma mudança

> **S** - Uma classe deve ter *um, e apenas um,* motivo para ser modificada

> **O** - Entidades de software (classes, módulos, funções, etc) devem estar abertas para extensão, mas fechada para modificação

> **L** - Subclasses devem ser substituíveis por suas Superclasses

> **I** - Clientes não devem ser forçados a depender de métodos que não usam 

> **D** - Dependa de uma abstração e não de uma implementação

* Módulos de *alto nível* não devem depender de módulos de *baixo nível*. Ambos devem depender de *abstrações*. 

* *Abstrações* não devem depender de *detalhes*. 

* *Detalhes* devem depender de *abstrações*.

---

### Microsserviços prontos para a produção
* ESTÁVEL
* CONFIAVEL
* ESCALAVEL
* TOLERANTE A FALHAS
* DE ALTO DESEMPENHO
* MONITORADO
* DOCUMENTADO
* PREPARADO PARA QUALQUER CATASTROFE
    
![](https://s3.novatec.com.br/capas-ampliadas/capa-ampliada-9788575226216.jpg)

[Microsserviços prontos para a produção](https://www.novatec.com.br/livros/microsservicos-para-producao/)

## **2021-10-07**

### Apache SPARK

>**RDDs:** abstração para processar coleções de dados de forma distibuída e resiliente a falhas.

>**Dataframes/Datasets:** abstração baseada em lógica relacional e implementada utilizando RDDs para processar dados de fontes diversas.

>**SparkML:** abstração de alto nível implementada em dataframes para desenvolver aplicações de aprendizado de máquina.

>**GraphX:** abstração de alto nível implementada em RDDs para desenvolver aplicações de processamento de grafos.

### Arquitetura do Spark e seus principais componentes.
![Arquitetura do Spark e seus principais componentes.](https://arquivo.devmedia.com.br/artigos/Eduardo_Zambom/spark/image002.jpg)

![Componentes do Apache Spark](https://arquivo.devmedia.com.br/artigos/Eduardo_Zambom/spark/image001.png)

Links:
* [Introdução ao Apache Spark](https://www.devmedia.com.br/introducao-ao-apache-spark/34178)
* [Spark Full Course | Spark Tutorial For Beginners | Learn Apache Spark | Simplilearn
](https://www.youtube.com/watch?v=S2MUhGA3lEw)

---

## **2021-10-06**

### JPA

#### Implementações da JPA

>No caso da *JPA*, a implementação mais famosa e utilizada no mercado é o *Hibernate*, mas existem mais no mercado como *OpenJPA*, *EclipseLink*, *Batoo* e outras. Cada implementação tem suas vantagens e desvantagens na hora da utilização.

#### Mapeamento
>*@Basic* - **fetch** indica se o conteúdo será carregado juntamente com a Entity quando ela for buscada no banco de dados.

#### @GeneratedValue
> A *JPA* tem 4 modos para gerar automaticamente o id de uma entidade: IDENTITY, SEQUENCE, TableGenerator e AUTO.
>
> O tipo de geração **IDENTITY** nada mais é do que o famoso autoincremento do banco de dados. Para as pessoas que trabalham com MySQL ou SQLServer. 
>
>* A estratégia **SEQUENCE** utiliza uma rotina que se encontra no banco de dados, que, ao ser chamada, devolve automaticamente o próximo número sem problemas de concorrência. Esse esquema é o padrão do Postgres e Oracle.
>
>* A estratégia **TableGenerator** salva todas as chaves em uma tabela. Essa tabela será composta de duas colunas, sendo que uma indica o nome da tabela e a outra, o valor do id atual para aquela tabela. 
>
>* O modo **AUTO** para gerar id é o mais simples de todos e, quando utilizado, a *JPA* ficará encarregada de escolher o modo como será realizada a geração do id. Esse é o valor padrão da anotação *@GeneratedValue*.
>
>* Caso o seu banco só dê suporte a *IDENTITY* você terá que usar ou *IDENTITY* ou *TABLE_GENERATOR*. 
><br>*TABLE_GENERATOR* é a única estratégia que pode ser utilizada com qualquer banco de dados. É preciso saber qual o modo de geração de seu banco de dados e utilizar.
> Se o seu projeto for ser utilizado por diversos bancos de dados, a questão da chave é um ponto importante para se levar em consideração.
> É possível perceber também que o tipo da chave escolhida pode influenciar na performance. É preciso ter sempre em mente qual a melhor escolha para o seu projeto.
>
>
>| @GeneratedValue(strategy = ?) | Utilização                          |
>|-------------------------------|-------------------------------------|
>| IDENTITY                      | MySQL ou SQLServer                  |  
>| SEQUENCE                      | Postgres e Oracle                   |  
>| TableGenerator                | Tabela                              |  
>| AUTO                          | JPA ficará encarregada de escolher  |  


caminho certo

se dedicar mais - com a lista
